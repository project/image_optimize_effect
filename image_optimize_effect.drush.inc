<?php

/**
 * @file
 * Drush interface to image optimize effect.
 */

/**
 * Implements hook_drush_command().
 */
function image_optimize_effect_drush_command() {
  $commands = array();
  $commands['image-optimize-effect-all'] = array(
    'description' => 'Optimize all generated images.',
    'callback' => 'drush_image_optimize_effect_all',
    'aliases' => array('ioea'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
    'arguments' => array(
      'limit' => dt("The number of images to convert. Set to 0 to process all images. Defaults to 0."),
      'speed' => dt('Speed/quality trade-off from 1 (brute-force) to 10 (fastest). The default is 3. Speed 10 has 5% lower quality, but is 8 times faster than the default.'),
    ),
  );
  $commands['image-optimize-effect-png'] = array(
    'description' => 'Optimize all generated png images.',
    'callback' => 'drush_image_optimize_effect_png',
    'aliases' => array('ioep'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
    'arguments' => array(
      'limit' => dt("The number of images to convert. Set to 0 to process all images. Defaults to 0."),
      'speed' => dt('Speed/quality trade-off from 1 (brute-force) to 10 (fastest). The default is 3. Speed 10 has 5% lower quality, but is 8 times faster than the default.'),
    ),
  );
  $commands['image-optimize-effect-jpg'] = array(
    'description' => 'Optimize all generated jpeg images.',
    'callback' => 'drush_image_optimize_effect_jpg',
    'aliases' => array('ioej'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
    'arguments' => array(
      'limit' => dt("The number of images to convert. Set to 0 to process all images. Defaults to 0."),
    ),
  );
  return $commands;
}

/**
 * Callback for command image-optimize-effect-all.
 */
function drush_image_optimize_effect_all($limit = NULL, $speed = 3) {
  drush_image_optimize_effect_png($limit, $speed);
  drush_image_optimize_effect_jpg($limit);
}

/**
 * Callback for command image-optimize-effect-png.
 */
function drush_image_optimize_effect_png($limit = NULL, $speed = 3) {
  $last_run = variable_get('image_optimize_effect_last_run', 0);
  $counter = 0;
  foreach(file_scan_directory('public://styles/', '/.png/') as $file) {
    if (!is_writable($file->uri)) {
      drush_log(dt('@file is not writable.', array(
        '@file' => $file->uri,
      )), 'ok');
    }
    else {
      $file_changed = filemtime($file->uri);
      if ($file_changed > $last_run) {
        $image = image_load($file->uri);
        drush_log(dt('Processing @file, current size is @size.', array(
          '@file' => $file->uri,
          '@size' => $image->info['file_size'],
        )), 'ok');
        if (image_optimize_effect_pngquant($image, array('speed' => $speed))) {
          image_save($image);
        }
        drush_log(dt('Done processing @file, new size is @size.', array(
          '@file' => $file->uri,
          '@size' => $image->info['file_size'],
        )), 'ok');
        imagedestroy($image->resource);

        // Check limit if needed.
        $counter++;
        if (isset($limit) && $counter >= $limit) {
          return;
        }
      }
    }
  }
  variable_set('image_optimize_effect_last_run', time());
}

/**
 * Callback for command image-optimize-effect-jpg.
 */
function drush_image_optimize_effect_jpg($limit = NULL) {
  $last_run = variable_get('image_optimize_effect_last_run', 0);
  // Recurse all files in styles.
  foreach(file_scan_directory('public://styles/', '/.jpg|.jpeg/') as $file) {
    if (!is_writable($file->uri)) {
      drush_log(dt('@file is not writable.', array(
        '@file' => $file->uri,
      )), 'ok');
    }
    else {
      $file_changed = filemtime($file->uri);
      if ($file_changed > $last_run) {
        $image = image_load($file->uri);
        drush_log(dt('Processing @file, current size is @size.', array(
          '@file' => $file->uri,
          '@size' => $image->info['file_size'],
        )), 'ok');
        if (image_optimize_effect_imgmin($image)) {
          image_save($image);
        }
        drush_log(dt('Done processing @file, new size is @size.', array(
          '@file' => $file->uri,
          '@size' => filesize($file->uri),
        )), 'ok');
        imagedestroy($image->resource);

        // Check limit if needed.
        $counter++;
        if (isset($limit) && $counter >= $limit) {
          return;
        }
      }
    }
  }
  variable_set('image_optimize_effect_last_run', time());
}

